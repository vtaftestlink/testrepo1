package com.regressionproject_01;

import com.virtusa.isq.vtaf.runtime.SeleniumTestBase;
import org.openqa.selenium.By;
import com.virtusa.isq.vtaf.utils.PropertyHandler;

/**
 *  Class lib_001 contains reusable business components 
 *  Each method in this class correspond to a reusable business component.
 */
public class lib_001 {

    /**
     *  Business component bc_getScreenCordinates.
     * 
     */
    /**
     *  Business component bc_getScreenCordinates.
     * 
     */
    public final static void bc_getScreenCordinates(final SeleniumTestBase caller) throws Exception {
        String dummy1 = "a";java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();
        int w = (int)width;
        int h = (int)height;
        String dummy2 = "a";org.openqa.selenium.WebElement Image = caller.getDriver().findElement(org.openqa.selenium.By.xpath("//input[@id='main-site-search']"));
        String dummy3 = "a";org.openqa.selenium.Point point = Image.getLocation();
        int xcord = point.getX();
        int ycord = point.getY();
        caller.store("key_w","Int",w);
        caller.store("key_h","Int",h);
        caller.store("key_x","Int",xcord);
        ycord = ycord*1;
        caller.store("key_y","Int",ycord);	
    }
    /**
     *  Business component bc_ebaySearch.
     * 
     */
    /**
     *  Business component bc_ebaySearch.
     * 
     */
    public final static void bc_ebaySearch(final SeleniumTestBase caller) throws Exception {
        //ebaySearch
        caller.open("http://www.ebay.com/","1000");
        caller.type("ebay_homePage.tf_search","Shoe");
        caller.click("ebay_homePage.btn_search");	
    }
}
