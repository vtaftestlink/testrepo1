package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class W3CNewWindow implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum W3CNewWindow {

        btn_tryItYourself("//a[contains(text(),'Try it Yourself')]");

    private String searchPath;
  
    /**
    *  Page W3CNewWindow.
    */
    private W3CNewWindow(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}