package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class W3CTable implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum W3CTable {

        page_tbl("//div[@class='w3-white w3-padding notranslate']");

    private String searchPath;
  
    /**
    *  Page W3CTable.
    */
    private W3CTable(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}