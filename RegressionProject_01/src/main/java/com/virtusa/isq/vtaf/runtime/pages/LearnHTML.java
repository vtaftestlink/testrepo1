package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class LearnHTML implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum LearnHTML {

        btn_learnHTML("//a[contains(text(),'Try it Yourself')]"), ta_htmlBody("//div[@id='textareawrapper']/div/div/textarea"), new_window("index=1"), current_window("index=0"), frm_textAreaContainer("//iframe[@id='iframeResult']"), ele_heading("//html/body/h1");

    private String searchPath;
  
    /**
    *  Page LearnHTML.
    */
    private LearnHTML(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}