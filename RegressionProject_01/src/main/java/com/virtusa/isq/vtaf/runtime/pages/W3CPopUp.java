package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class W3CPopUp implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum W3CPopUp {

        lnk_popup("//button[contains(text(),'Try it')]"), frm_iframe("//iframe[@id=\"iframeResult\"]"), dgvd("//button[contains(text(),'Try it')]..//button[contains(text(),'Try it')]");

    private String searchPath;
  
    /**
    *  Page W3CPopUp.
    */
    private W3CPopUp(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}