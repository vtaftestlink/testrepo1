package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class HTMLTable implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum HTMLTable {

        tbl_rows("//table/tbody/tr"), tbl_Main("//table");

    private String searchPath;
  
    /**
    *  Page HTMLTable.
    */
    private HTMLTable(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}