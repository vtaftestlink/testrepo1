package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class IEEE_Home implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum IEEE_Home {

        lnk_about("//a[text()='About']"), tf_search("//input[@id='main-site-search']");

    private String searchPath;
  
    /**
    *  Page IEEE_Home.
    */
    private IEEE_Home(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}