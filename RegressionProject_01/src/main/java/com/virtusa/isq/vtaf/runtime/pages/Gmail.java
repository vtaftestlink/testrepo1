package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class Gmail implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum Gmail {

        SignIn_Btn("//a[@tabindex='1']");

    private String searchPath;
  
    /**
    *  Page Gmail.
    */
    private Gmail(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}