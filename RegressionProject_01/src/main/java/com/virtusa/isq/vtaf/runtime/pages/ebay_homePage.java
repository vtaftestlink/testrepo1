package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class Ebay_homePage implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum ebay_homePage {

        dd_catogory("//select[@id='gh-cat']"), btn_search("//input[@id='gh-btn']"), tf_search("//input[@id='gh-ac']"), ele_lblListItemObject("//td[@role='listitem']"), frm_iframe("//iframe[@id=\"hiddenGpt\"]"), ele_ddCatagory("//select[@id=\"gh-cat\"]"), ele_following("//a[text()='Following']"), win_windows("index=<index>");

    private String searchPath;
  
    /**
    *  Page ebay_homePage.
    */
    private ebay_homePage(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}