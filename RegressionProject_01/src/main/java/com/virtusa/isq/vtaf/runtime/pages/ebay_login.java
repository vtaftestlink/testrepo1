package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class Ebay_login implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum ebay_login {

        tf_username("//input[@name='1259929423'] "), tf_password("//input[@id=\"2017017033\"] "), win_window("index=<index>");

    private String searchPath;
  
    /**
    *  Page ebay_login.
    */
    private ebay_login(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}