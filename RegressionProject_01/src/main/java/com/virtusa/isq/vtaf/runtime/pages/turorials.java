package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class Turorials implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum turorials {

        ele_htmlAndCss("//*[text()='HTML and CSS']/../../a"), lnk_learnHTML("//div/h4[contains(text(),'HTML and CSS')]/../../a[contains(text(),'Learn HTML')]"), lnk_learnCSS("//div/h4[contains(text(),'HTML and CSS')]/../../a[contains(text(),'Learn CSS')]");

    private String searchPath;
  
    /**
    *  Page turorials.
    */
    private turorials(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}