package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class Ebay implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum ebay {

        dd_catogory("//select[@id='gh-cat']"), btn_search("//input[@id='gh-btn']"), tf_search("//input[@id='gh-ac']");

    private String searchPath;
  
    /**
    *  Page ebay.
    */
    private ebay(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}