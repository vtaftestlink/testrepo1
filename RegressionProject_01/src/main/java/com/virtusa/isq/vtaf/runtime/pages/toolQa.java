package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class ToolQa implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum toolQa {

;

    private String searchPath;
  
    /**
    *  Page toolQa.
    */
    private toolQa(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}