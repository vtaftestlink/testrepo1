package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class Microsoft implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum Microsoft {

        chk_rememberLogin("//input[@id='cred_keep_me_signed_in_checkbox']");

    private String searchPath;
  
    /**
    *  Page Microsoft.
    */
    private Microsoft(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}