package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class JsPopups implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum JsPopups {

        btn_TriItYourSelf("//div[@class='w3-example'][1]/h3/../a"), btn_tryIT("//button[text()='Try it']");

    private String searchPath;
  
    /**
    *  Page JsPopups.
    */
    private JsPopups(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}