package com.regressionproject_01;

import java.util.HashMap;
import java.util.List;

import com.virtusa.isq.vtaf.aspects.VTAFRecoveryMethods;
import com.virtusa.isq.vtaf.runtime.SeleniumTestBase;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import com.virtusa.isq.vtaf.runtime.VTAFTestListener;
import com.virtusa.isq.vtaf.utils.PropertyHandler;


/**
 *  Class ts_RegressionPass implements corresponding test suite
 *  Each test case is a test method in this class.
 */

@Listeners (VTAFTestListener.class)
public class ts_RegressionPass extends SeleniumTestBase {



    /**
     * Data provider for Test case tc_Pass.
     * @return data table
     */
    @DataProvider(name = "tc_Pass")
    public Object[][] dataTable_tc_Pass() {     	
    	return this.getDataTable("dt_test_001","dt_test_002");
    }

    /**
     * Data driven test case tc_Pass.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {}) 
    @Test (dataProvider = "tc_Pass")
    public final void tc_Pass(final String dt_test_001_ThisIsHeading, final String dt_test_001_VerificationText, final String dt_test_001_url, final String dt_test_001_searchData, final String dt_test_001_XMLPath, final String dt_test_002_ElementText, final String dt_test_002_pageUrl) throws Exception {
    	
    	//sasasa
    	/*
    	NavigateToURL
    	 url=HTMLTable
    	 ms=2000
    	Pause
    	 ms=18000
    	 comment=null
    	CheckTable
    	 object=HTMLTable.tbl_Main
    	 validationType=ROWCOUNT
    	 expectedValue=2
    	 stopOnFailure=false
    	 customErrorMessage=
    	NavigateToURL
    	 url=IEEE_Home
    	 ms=2000
    	Pause
    	 ms=2000
    	 comment=null
    	MouseOver
    	 object=IEEE_Home.tf_search
    	Pause
    	 ms=3000
    	 comment=null
    	Call
    	 businessComponent=lib_001.bc_getScreenCordinates
    	Pause
    	 ms=2000
    	 comment=null
    	Retrieve
    	 key=key_w
    	 var=w
    	 type=Int
    	Retrieve
    	 key=key_h
    	 var=h
    	 type=Int
    	Retrieve
    	 key=key_x
    	 var=xcord
    	 type=Int
    	Retrieve
    	 key=key_y
    	 var=ycord
    	 type=Int
    	MouseMoveAndClick
    	 screenSize=@w+","+h
    	 coordinates=@xcord+","+ycord
    	 ms=5000
    	Pause
    	 ms=2000
    	 comment=null
    	Type
    	 object=IEEE_Home.tf_search
    	 text=machine learning
    	FireEvent
    	 event=KEY%key=\n
    	 waitTime=2000
    	Screenshot
    	 name=SearchContent
    	Pause
    	 ms=2000
    	 comment=null
    	Screenshot
    	 name=SearchContent
    	Pause
    	 ms=2000
    	 comment=null
    	NavigateToURL
    	 url=Microsoft
    	 ms=3000
    	DoubleClick
    	 object=Microsoft.chk_rememberLogin
    	Pause
    	 ms=2000
    	 comment=null
    	DoubleClickAt
    	 object=Microsoft.chk_rememberLogin
    	 coordinates=0,0
    	Pause
    	 ms=2000
    	 comment=null
    	NavigateToURL
    	 url=ebay
    	 ms=3000
    	SetVariable
    	 name=arrSearch []
    	 type=String
    	 paramValue=@dt_test_001_searchData.split(",")
    	Loop
    	 expression=int x=0;x<arrSearch.length;x++
    	 Select
    	  object=ebay.dd_catogory
    	  selectLocator=@arrSearch[x]
    	 Click
    	  object=ebay.btn_search
    	 Pause
    	  ms=4000
    	  comment=null
    	EndLoop
    	KeyPress
    	 object=ebay.tf_search
    	 value=i|p|h|o|n|e
    	Pause
    	 ms=2000
    	 comment=null
    	Click
    	 object=ebay.btn_search
    	CreateUser
    	 username=testUser
    	 browser=firefox
    	 serverConfig=
    	Pause
    	 ms=2000
    	 comment=null
    	Pause
    	 ms=2000
    	 comment=null
    	Open
    	 page=toolQa
    	 ms=2000
    	SwitchUser
    	 username=testUser
    	CheckDocument
    	 docType=XML
    	 filePath=@dt_test_001_XMLPath
    	 pageNumberRange=1
    	 verifyType=NODECOUNT
    	 inputString=Table|1
    	 stopOnFailure=false
    	 customErrorMessage=
    	WriteToReport
    	 comment=End of TC Regression
    	*/
    	
    } 
    	

    /**
     * Data provider for Test case tc_RegressionPass.
     * @return data table
     */
    @DataProvider(name = "tc_RegressionPass")
    public Object[][] dataTable_tc_RegressionPass() {     	
    	return this.getDataTable("dt_test_001");
    }

    /**
     * Data driven test case tc_RegressionPass.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {}) 
    @Test (dataProvider = "tc_RegressionPass")
    public final void tc_RegressionPass(final String dt_test_001_ThisIsHeading, final String dt_test_001_VerificationText, final String dt_test_001_url, final String dt_test_001_searchData, final String dt_test_001_XMLPath) throws Exception {
    	
    	/*
    	//asas
    	WriteToReport
    	 comment=Start of TC Regression
    	//Open W3C home page
    	//give http://www.w3schools.com/ from the runtime properties under default url feild
    	Open
    	 page=W3CHome
    	 ms=2000
    	CheckElementPresent
    	 object=W3CHome.lnk_ele_tutorials
    	 stopOnFailure=false
    	 customErrorMessage=Element not present
    	CheckObjectProperty
    	 object=W3CHome.lnk_ele_tutorials
    	 propertyname=id
    	 expectedValue=navbtn_tutorials
    	 stopOnFailure=false
    	 customErrorMessage=
    	Click
    	 object=W3CHome.lnk_ele_tutorials
    	//get object count on HTML and CSS Sub topics
    	GetObjectCount
    	 object=turorials.ele_htmlAndCss
    	 varName=Var_htmlAndCssSubCount
    	//click on tutorials link
    	Click
    	 object=W3CHome.lnk_ele_tutorials
    	Pause
    	 ms=2000
    	 comment=null
    	ClickAt
    	 object=turorials.lnk_learnHTML
    	 coordinates=0,0
    	Click
    	 object=LearnHTML.btn_learnHTML
    	Pause
    	 ms=2000
    	 comment=null
    	//get focus in to newly opened window
    	SelectWindow
    	 object=LearnHTML.new_window
    	Pause
    	 ms=2000
    	 comment=null
    	CheckWindowProperty
    	 object=LearnHTML.new_window
    	 windowproperty=WINDOWPRESENT
    	 expectedValue=true
    	 stopOnFailure=false
    	 customErrorMessage=
    	SelectFrame
    	 object=LearnHTML.frm_textAreaContainer
    	Pause
    	 ms=3000
    	 comment=null
    	SetVarProperty
    	 var=var_Heading
    	 type=String
    	 object=LearnHTML.ele_heading
    	 property=TEXT:
    	If
    	 expression=@dt_test_001_ThisIsHeading.equals(var_Heading)
    	 WriteToReport
    	  comment=if command executed
    	EndIf
    	If
    	 expression=!dt_test_001_ThisIsHeading.equals(var_Heading)
    	Else
    	 WriteToReport
    	  comment=Else command executed
    	EndIf
    	If
    	 expression=!dt_test_001_ThisIsHeading.equals(var_Heading)
    	ElseIf
    	 expression=@dt_test_001_ThisIsHeading.equals(var_Heading)
    	 WriteToReport
    	  comment=ElseIF command executed
    	EndIf
    	Screenshot
    	 name=TestImage
    	//close newly opened window
    	FireEvent
    	 event=KEY%key=ctrl+w
    	 waitTime=2000
    	Pause
    	 ms=1000
    	 comment=null
    	SelectWindow
    	 object=LearnHTML.current_window
    	GoBack
    	 ms=2000
    	CheckPattern
    	 object=W3CHome.ele_HTML
    	 pattern=[A-Z][A-Z][A-Z][A-Z]
    	 stopOnFailure=false
    	 customErrorMessage=
    	StartDataIteration
    	 tables=dt_test_002
    	 dataRows=All
    	 WriteToReport
    	  comment=StartDataIteration Command executed
    	 CheckTextPresent
    	  verificationText=@row_dataMap1.get("dt_test_002_ElementText")
    	  stopOnFailure=false
    	  customErrorMessage=
    	EndDataIteration
    	WriteToReport
    	 comment=EndDataIteration Command executed
    	RightClick
    	 object=turorials.lnk_learnCSS
    	Pause
    	 ms=2000
    	 comment=null
    	FireEvent
    	 event=KEY%key=esc
    	 waitTime=2000
    	Pause
    	 ms=3000
    	 comment=null
    	Screenshot
    	 name=rightClicked
    	Pause
    	 ms=2000
    	 comment=null
    	NavigateToURL
    	 url=JsPopups
    	 ms=3000
    	Click
    	 object=JsPopups.btn_TriItYourSelf
    	Pause
    	 ms=1000
    	 comment=null
    	SelectWindow
    	 object=LearnHTML.new_window
    	Pause
    	 ms=2000
    	 comment=null
    	SelectFrame
    	 object=LearnHTML.frm_textAreaContainer
    	Pause
    	 ms=2000
    	 comment=null
    	Click
    	 object=JsPopups.btn_tryIT
    	Pause
    	 ms=1000
    	 comment=null
    	HandlePopup
    	 inputString=Action=OK
    	 waitTime=3000
    	 stopOnFailure=null
    	FireEvent
    	 event=KEY%key=ctrl+w
    	 waitTime=3000
    	Pause
    	 ms=2000
    	 comment=null
    	SelectWindow
    	 object=LearnHTML.current_window
    	Pause
    	 ms=2000
    	 comment=null
    	*/
    	
    } 
    	

    public final Object[][] getDataTable(final String... tableNames) {
        String[] tables = tableNames;
        return this.getTableArray(getVirtualDataTable(tables));
    }

}