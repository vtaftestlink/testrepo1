package com.regressionproject_01;

import java.util.HashMap;
import java.util.List;

import com.virtusa.isq.vtaf.aspects.VTAFRecoveryMethods;
import com.virtusa.isq.vtaf.runtime.SeleniumTestBase;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import com.virtusa.isq.vtaf.runtime.VTAFTestListener;
import com.virtusa.isq.vtaf.utils.PropertyHandler;


/**
 *  Class ts_Commands implements corresponding test suite
 *  Each test case is a test method in this class.
 */

@Listeners (VTAFTestListener.class)
public class ts_Commands extends SeleniumTestBase {



    /**
     * Test case tc_KeyPress.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_KeyPress() throws Exception {

    	//KeyPress
    	open("http://www.ebay.com/","4000");
    	keyPress("ebay_homePage.tf_search","S|E|L|E|N|I|U|M| |T|E|S|T");
    	keyPress("ebay_homePage.tf_search","S|E|L|E|N|I|U|M| |T|E|S|T|2");
    	keyPress("ebay_homePage.tf_search","S|E|L|E|N|I|U|M| |T|E|S|T|3");
    	
    } 
    	

    /**
     * Test case tc_GetObjectCount.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_GetObjectCount() throws Exception {

    	//tc_GetObjectCount
    	open("http://www.ebay.com/","4000");
    	int var_Count01 = getObjectCount("ebay_homePage.tf_search");          
    	writeToReport(var_Count01);
    	int var_Count02 = getObjectCount("ebay_homePage.ele_lblListItemObject");          
    	writeToReport(var_Count02);
    	int var_Count03 = getObjectCount("ebay_homePage.dd_catogory");          
    	writeToReport(var_Count03);
    	
    } 
    	

    /**
     * Data provider for Test case tc_CheckDocument.
     * @return data table
     */
    @DataProvider(name = "tc_CheckDocument")
    public Object[][] dataTable_tc_CheckDocument() {     	
    	return this.getDataTable("dt_test_001","dt_test_002");
    }

    /**
     * Data driven test case tc_CheckDocument.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {}) 
    @Test (dataProvider = "tc_CheckDocument")
    public final void tc_CheckDocument(final String dt_test_001_ThisIsHeading, final String dt_test_001_VerificationText, final String dt_test_001_url, final String dt_test_001_searchData, final String dt_test_001_XMLPath, final String dt_test_002_ElementText, final String dt_test_002_pageUrl) throws Exception {
    	
    	//
    	//tc_CheckDocument
    	checkDocument("EXCEL","C:\\Keystone\\Research\\VTAF_Regression_Test\\Documents\\SampleExcel\\CompanySearch.xlsx","exportCompanyTable","TABLECELL","0,0,Company ID",false,"error on testing");
    	/*
    	Fail
    	 message=error
    	Loop
    	 expression=
    	 Screenshot
    	  name=
    	 CheckPattern
    	  object=ebay_homePage.ele_lblListItemObject
    	  pattern=
    	  stopOnFailure=true
    	  customErrorMessage=error
    	 CheckPattern
    	  object=ebay_homePage.ele_lblListItemObject
    	  pattern=XX
    	  stopOnFailure=false
    	  customErrorMessage=error
    	 NavigateToURL
    	  url=ebay_homePage
    	  ms=1000
    	 Type
    	  object=ebay.tf_search
    	  text=@dt_test_001_url
    	 TypeKey
    	  object=ebay_homePage.tf_search
    	  text=shoe
    	 DoubleClick
    	  object=ebay.btn_search
    	 RightClick
    	  object=ebay_homePage.ele_lblListItemObject
    	 Call
    	  businessComponent=lib_001.bc_getScreenCordinates
    	 SwitchUser
    	  username=@dt_test_001_ThisIsHeading
    	 ClickAt
    	  object=W3CHome.lnk_ele_references
    	 Click
    	  object=
    	 EditVariable
    	  name=
    	 CheckElementPresent
    	  object=W3CHome.
    	 IsCheckElementPresent
    	  object=W3CHome.
    	  name=
    	  stopOnFailure=
    	  customErrorMessage=
    	  paramValue=
    	 GoBack
    	  ms=
    	 Select
    	  object=
    	 Retrieve
    	  key=

    	 SetVarProperty
    	  var=
    	 DoubleClickAt
    	  object=
    	  coordinates=
    	  type=
    	  object=
    	  property=  var=
    	  type=
    	  selectLocator=
    	 CheckTextPresent
    	  verificationText=
    	  stopOnFailure=
    	  customErrorMessage=
    	 SelectFrame
    	  object=
    	 WriteToReport
    	  comment=dd
    	 HandlePopup
    	  inputString=
    	  waitTime=
    	 SelectWindow
    	  object=W3CHome.lnk_ele_tutorials
    	 FireEvent
    	  event=
    	  waitTime=
    	 CheckTable
    	  object=
    	  validationType=
    	  expectedValue=
    	  stopOnFailure=
    	  customErrorMessage=

    	  CreateUser
    	  username=
    	  browser=
    	  serverConfig=CreateUser
    	  username=
    	  browser=
    	  serverConfig=
    	 SetVariable
    	  name=
    	  type=
    	  paramValue=

    	 Store
    	  key=
    	 Pause
    	  ms=
    	 CheckObjectProperty
    	  object=W3CHome.lnk_ele_tutorials
    	 MouseOver
    	  object=
    	 Open
    	  page=
    	 CheckWindowProperty
    	  object=

    	  windowproperty=
    	  expectedValue=
    	  stopOnFailure=
    	  customErrorMessage=
    	  ms=
    	  propertyname=
    	  expectedValue=
    	  stopOnFailure=
    	  customErrorMessage=
    	  type=
    	  value=

    	 CheckDocument
    	  docType=
    	  filePath=
    	  pageNumberRange=
    	  verifyType=
    	  inputString=
    	  stopOnFailure=
    	  customErrorMessage=
    	 HandleImagePopup
    	  imagePath=
    	  actionFlow=
    	  waitTime=

    	 CheckImagePresent
    	  object=
    	  isRotatable=
    	  stopOnFailure=
    	  customErrorMessage=
    	 CheckSorting
    	  object=
    	  sortType=
    	  sortPattern=
    	  sortOrder=
    	  stopOnFailure=
    	  customErrorMessage=

    	 ResizeApplication
    	  actions=
    	  coordinates=
    	 GetElementInfo
    	  object=
    	  varName=


    	 Switch
    	  expression=
    	 EndSwitch
    	 Case
    	  expression=
    	 Break
    	 WaitForElementEnable
    	  object=
    	 CloseWindow
    	*/
    	
    } 
    	

    /**
     * Test case tc_fail.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_fail() throws Exception {

    	//Fail
    	open("https://www.w3schools.com","1000");
    	fail("error");
    	
    } 
    	

    /**
     * Test case tc_Loop.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_Loop() throws Exception {

    	//tc_Loop
    	int var_dami = 10;
    	for(int i = 0; i<var_dami;i++){
    	writeToReport(var_dami+"var_Value ");
    	}
    	if(var_dami!=10){
    	fail("Count is not matched !.");
    	}
    	
    } 
    	

    /**
     * Test case tc_Screenshot.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_Screenshot() throws Exception {

    	//Screenshot
    	open("http://www.ebay.com/","1000");
    	screenshot("kamal");
    	
    } 
    	

    /**
     * Test case tc_CheckPattern.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_CheckPattern() throws Exception {

    	//CheckPattern
    	checkPattern("W3CHome.lnk_ele_examples","",true,"error");
    	checkPattern("ebay_homePage.ele_lblListItemObject","",false,"error");
    	
    } 
    	

    /**
     * Data provider for Test case tc_NavigateToURL.
     * @return data table
     */
    @DataProvider(name = "tc_NavigateToURL")
    public Object[][] dataTable_tc_NavigateToURL() {     	
    	return this.getDataTable("dt_test_001");
    }

    /**
     * Data driven test case tc_NavigateToURL.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {}) 
    @Test (dataProvider = "tc_NavigateToURL")
    public final void tc_NavigateToURL(final String dt_test_001_ThisIsHeading, final String dt_test_001_VerificationText, final String dt_test_001_url, final String dt_test_001_searchData, final String dt_test_001_XMLPath) throws Exception {
    	
    	//NavigateToURL
    	navigateToURL("http://www.ebay.com/","2000");
    	
    } 
    	

    /**
     * Test case tc_Type.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_Type() throws Exception {

    	//Type
    	open("http://www.ebay.com/","1000");
    	type("ebay.tf_search","shoe");
    	
    } 
    	

    /**
     * Test case tc_TypeKey.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_TypeKey() throws Exception {

    	//TypeKey
    	open("http://www.ebay.com/","1000");
    	typeKey("ebay_homePage.tf_search","shoe");
    	
    } 
    	

    /**
     * Test case tc_DoubleClick.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_DoubleClick() throws Exception {

    	//DoubleClick
    	open("https://www.w3schools.com","1000");
    	doubleClick("W3CHome.ele_HTML");
    	
    } 
    	

    /**
     * Test case tc_RightClick.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_RightClick() throws Exception {

    	//RightClick
    	open("http://www.w3schools.com/js/js_popup.asp","1000");
    	rightClick("JsPopups.btn_TriItYourSelf");
    	
    } 
    	

    /**
     * Test case tc_Call.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_Call() throws Exception {

    	//Call
    	open("http://www.ebay.com/","1000");
    	lib_001.bc_ebaySearch(this);
    	
    } 
    	

    /**
     * Test case tc_SwitchUser.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_SwitchUser() throws Exception {

    	//SwitchUser
    	open("https://signin.ebay.com/ws/eBayISAPI.dll?SignIn&ru=http%3A%2F%2Fwww.ebay.com%2F","3000");
    	checkElementPresent("ebay_login.tf_username",false,"");
    	createUser("userOne","firefox","");
    	switchUser("userOne");
    	open("https://www.w3schools.com","3000");
    	checkElementPresent("W3CHome.ele_HTML",false,"");
    	
    } 
    	

    /**
     * Test case tc_ClickAt.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_ClickAt() throws Exception {

    	//clickat
    	open("https://www.w3schools.com","1000");
    	clickAt("W3CHome.lnk_ele_examples","1,1");
    	
    } 
    	

    /**
     * Test case tc_Click.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_Click() throws Exception {

    	//Click
    	open("https://www.w3schools.com","1000");
    	click("W3CHome.lnk_ele_tutorials");
    	
    } 
    	

    /**
     * Test case tc_EditVariable.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_EditVariable() throws Exception {

    	//EditVariable
    	int var_dummy1 = 0;
    	var_dummy1 = var_dummy1+10;
    	if(var_dummy1 !=10){
    	fail(var_dummy1+" value is not equale to 10.");
    	} else {
    	writeToReport(var_dummy1 +"  value is equale to 10.");
    	}
    	//--String
    	String var_dummy2 = "abc";
    	var_dummy2 = var_dummy2+"10";
    	if(!var_dummy2.equals("abc10")){
    	fail(var_dummy2 +"value is not equale to 10 .");
    	} else {
    	writeToReport(var_dummy2 +"  value is equale to 10.");
    	}
    	//--Boolean
    	boolean var_dummy3 = true;
    	var_dummy3 = false;
    	if(var_dummy3){
    	fail(var_dummy3 +"value is  false.");
    	} else {
    	writeToReport(var_dummy3 +"  value is true.");
    	}
    	//--Double
    	double var_dummy4 = 0.0;
    	var_dummy4 = var_dummy4+10;
    	if(var_dummy4 !=10){
    	fail(var_dummy4+ " value is not equale to 10 .");
    	} else {
    	writeToReport(var_dummy4 + "value is equale to 10 .");
    	}
    	
    } 
    	

    /**
     * Test case tc_CheckElementPresent.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_CheckElementPresent() throws Exception {

    	//CheckElementPresent
    	open("http://www.ebay.com/","1000");
    	checkElementPresent("ebay_homePage.tf_search",true,"error");
    	
    } 
    	

    /**
     * Test case tc_GoBack.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_GoBack() throws Exception {

    	//GoBack
    	open("https://www.w3schools.com","1000");
    	goBack("1000");
    	
    } 
    	

    /**
     * Data provider for Test case tc_Select.
     * @return data table
     */
    @DataProvider(name = "tc_Select")
    public Object[][] dataTable_tc_Select() {     	
    	return this.getDataTable("dt_test_001");
    }

    /**
     * Data driven test case tc_Select.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {}) 
    @Test (dataProvider = "tc_Select")
    public final void tc_Select(final String dt_test_001_ThisIsHeading, final String dt_test_001_VerificationText, final String dt_test_001_url, final String dt_test_001_searchData, final String dt_test_001_XMLPath) throws Exception {
    	
    	//Select
    	open("http://www.ebay.com/","1000");
    	select("ebay_homePage.ele_ddCatagory","Baby");
    	
    } 
    	

    /**
     * Test case tc_Retrieve.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_Retrieve() throws Exception {

    	//tc_Retrieve
    	open("http://www.ebay.com/","1000");
    	store("key_ebay","String","ebay");
    	String var_dummy = retrieveString("key_ebay");
    	writeToReport(var_dummy);
    	
    } 
    	

    /**
     * Test case tc_MouseMoveAndClick.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_MouseMoveAndClick() throws Exception {

    	//MouseMoveAndClick
    	open("http://www.ebay.com/","1000");
    	mouseMoveAndClick("1366,768","681,181","1000");
    	
    } 
    	

    /**
     * Test case tc_SetVarProperty.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_SetVarProperty() throws Exception {

    	//SetVarProperty
    	open("http://www.ebay.com/","1000");
    	String var1 = getStringProperty("ebay_homePage.ele_lblListItemObject","textContent");
    	
    } 
    	

    /**
     * Test case tc_DoubleClickAt.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_DoubleClickAt() throws Exception {

    	//DoubleClickAt
    	open("http://www.ebay.com/","1000");
    	doubleClickAt("ebay_homePage.btn_search","375,450");
    	
    } 
    	

    /**
     * Test case tc_CheckTextPresent.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_CheckTextPresent() throws Exception {

    	//CheckTextPreset
    	open("http://www.ebay.com/","1000");
    	checkTextPresent("Following",true,"error");
    	
    } 
    	

    /**
     * Test case tc_SelectFrame.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_SelectFrame() throws Exception {

    	//SelectFrame
    	open("http://www.ebay.com/","1000");
    	selectFrame("ebay_homePage.frm_iframe");
    	
    } 
    	

    /**
     * Test case tc_WriteToReport.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_WriteToReport() throws Exception {

    	//WriteToReport
    	open("https://www.w3schools.com","1000");
    	writeToReport("RegressionPack");
    	
    } 
    	

    /**
     * Test case tc_HandlePopup.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_HandlePopup() throws Exception {

    	//HandlePopup
    	open("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_alert","1000");
    	selectFrame("W3CPopUp.frm_iframe");
    	click("W3CPopUp.lnk_popup");
    	handlePopup("action=ok","2000",true);
    	pause("10000","null");
    	
    } 
    	

    /**
     * Test case tc_SelectWindow.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_SelectWindow() throws Exception {

    	//SelectWindow
    	open("https://www.w3schools.com/jsref/met_win_open.asp","1000");
    	click("W3CNewWindow.btn_tryItYourself");
    	selectWindow("W3CHome.win_windows","index_PARAM:1");
    	
    } 
    	

    /**
     * Test case tc_FireEvent.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_FireEvent() throws Exception {

    	//FireEvent
    	open("https://www.w3schools.com","1000");
    	//--KeyInput
    	fireEvent("KEY%key=\t|wait=2000|type=regressionPack|wait=2000","1000");
    	//--MouseMiove
    	fireEvent("MOUSE%scroll=2|wait=2000|scroll=-2|wait=2000","1000");
    	//--VerifyText
    	fireEvent("VERIFY%regressionPack","2000");
    	
    } 
    	

    /**
     * Test case tc_CheckTable.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_CheckTable() throws Exception {

    	//CheckTable
    	open("https://www.w3schools.com/html/html_tables.asp","1000");
    	//---COLCOUNT
    	checkTable("W3CTable.page_tbl","COLCOUNT","3",true,"error");
    	//---ROWCOUNT
    	checkTable("W3CTable.page_tbl","ROWCOUNT","7",true,"error");
    	//---TABLEDATA
    	checkTable("W3CTable.page_tbl","TABLEDATA","Alfreds Futterkiste,Maria Anders,Germany",true,"error");
    	//---RELATIVE
    	checkTable("W3CTable.page_tbl","RELATIVE","Ernst Handel,1,Roland Mendel",true,"error");
    	//---TABLECELL
    	checkTable("W3CTable.page_tbl","TABLECELL","3,2,Austria",true,"error");
    	
    } 
    	

    /**
     * Test case tc_CreateUser.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_CreateUser() throws Exception {

    	//CreateUser
    	createUser("Newuser","chrome","");
    	open("https://www.w3schools.com","3000");
    	checkElementPresent("W3CHome.lnk_ele_tutorials",false,"");
    	
    } 
    	

    /**
     * Test case tc_SetVariable.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_SetVariable() throws Exception {

    	//SetVariable
    	String var_name = "Ebay";
    	writeToReport(var_name);
    	
    } 
    	

    /**
     * Test case tc_Store.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_Store() throws Exception {

    	//Store
    	store("5","int","50");
    	
    } 
    	

    /**
     * Test case tc_Pause.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_Pause() throws Exception {

    	//Pause
    	open("https://www.w3schools.com","1000");
    	pause("1000","Null");
    	
    } 
    	

    /**
     * Test case tc_CheckObjectProperty.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_CheckObjectProperty() throws Exception {

    	//CheckObjectProperty
    	open("http://www.ebay.com/","1000");
    	//--SELECTEDOPTION
    	checkObjectProperty("ebay.dd_catogory","SELECTEDOPTION","All Categories",false,"");
    	//--ALLOPTIONS
    	checkObjectProperty("ebay.dd_catogory","ALLOPTIONS","All Categories, Antiques, Art, Baby, Books, Business & Industrial, Cameras & Photo, Cell Phones & Accessories, Clothing, Shoes & Accessories, Coins & Paper Money, Collectibles, Computers/Tablets & Networking, Consumer Electronics, Crafts, Dolls & Bears, DVDs & Movies, eBay Motors, Entertainment Memorabilia, Gift Cards & Coupons, Health & Beauty, Home & Garden, Jewelry & Watches, Music, Musical Instruments & Gear, Pet Supplies, Pottery & Glass, Real Estate, Specialty Services, Sporting Goods, Sports Mem, Cards & Fan Shop, Stamps, Tickets & Experiences, Toys & Hobbies, Travel, Video Games & Consoles, Everything Else",false,"error");
    	//--MISSINGOPTION
    	checkObjectProperty("ebay.dd_catogory","MISSINGOPTION","Link 5",false,"error");
    	//--ELEMENTPRESENT
    	checkObjectProperty("ebay.dd_catogory","ELEMENTPRESENT","true",false,"error");
    	//---PROPERTYPRESENT
    	checkObjectProperty("ebay.dd_catogory","PROPERTYPRESENT","class|true",false,"error");
    	checkObjectProperty("ebay.dd_catogory","DISPLAY","true",false,"error");
    	
    } 
    	

    /**
     * Test case tc_MouseOver.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_MouseOver() throws Exception {

    	//MouseOver
    	open("https://www.google.com/gmail/about/","1000");
    	mouseOver("Gmail.SignIn_Btn");
    	
    } 
    	

    /**
     * Test case tc_Open.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_Open() throws Exception {

    	//Open
    	open("https://www.w3schools.com","2000");
    	
    } 
    	

    /**
     * Test case tc_CheckWindowProperty.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_CheckWindowProperty() throws Exception {

    	//CheckWindowProperty
    	open("http://www.ebay.com/","1000");
    	checkWindowProperty("ebay_homePage.win_windows","index_PARAM:0","WINDOWPRESENT","false",true,"error");
    	
    } 
    	

    /**
     * Test case tc_ResizeApplication.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_ResizeApplication() throws Exception {

    	//ResizeApplication
    	open("https://www.google.com/gmail/about/","1000");
    	resizeApplication("MAXIMIZE","0,0");
    	resizeApplication("RESIZE","700,500");
    	
    } 
    	

    /**
     * Test case tc_GetElementInfo.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_GetElementInfo() throws Exception {

    	//GetElementInfo
    	/*
    	GetElementInfo
    	 object=
    	 varName=var_temp1
    	*/
    	
    } 
    	

    /**
     * Test case tc_Switch.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_Switch() throws Exception {

    	//
    	int month = 2;
    	String monthString = "";
    	switch (month) {
    	case 1 :
    	monthString = "January";
    	break;
    	case 2 :
    	monthString = "February";
    	break;
    	case 3 :
    	monthString = "March";
    	break;
    	case 4 :
    	monthString = "April";
    	break;
    	}
    	String dummy = "a";System.out.println("Month is : "+monthString);
    	
    } 
    	

    /**
     * Test case tc_ClsoeWindow.
     *
     * @throws Exception the exception
     */
    @VTAFRecoveryMethods(onerrorMethods = {}, recoveryMethods = {})
    @Test
    public final void tc_ClsoeWindow() throws Exception {

    	//ClsoeWindow
    	open("https://www.w3schools.com","1000");
    	closeWindow();
    	
    } 
    	

    public final Object[][] getDataTable(final String... tableNames) {
        String[] tables = tableNames;
        return this.getTableArray(getVirtualDataTable(tables));
    }

}