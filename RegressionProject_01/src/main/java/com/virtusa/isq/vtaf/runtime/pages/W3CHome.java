package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class W3CHome implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum W3CHome {

        lnk_ele_tutorials("//a[@id='navbtn_tutorials']"), lnk_ele_references("//a[@id='navbtn_references']"), lnk_ele_examples("//a[@id='navbtn_examples']"), ele_HTML("//div/div/div/h1[contains(text(),'HTML')]"), lnk_eleLearnHtml("//a[@href=\"/html/default.asp\"]"), win_windows("index=<index>");

    private String searchPath;
  
    /**
    *  Page W3CHome.
    */
    private W3CHome(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}