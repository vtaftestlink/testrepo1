package com.virtusa.isq.vtaf.runtime.pages;

/**
 *  Class W3CDropDown implements corresponding UI page
 *  UI objects in the page are stored in the class.
 */

public enum W3CDropDown {

        ele_ddDropdown("//button[@class=\"dropbtn\"]");

    private String searchPath;
  
    /**
    *  Page W3CDropDown.
    */
    private W3CDropDown(final String psearchPath) {
        this.searchPath = psearchPath;
    }
    
    /**
     *  Get search path.
     * @param searchPath search path.
     */
    public final String getSearchPath() {
        return searchPath;
    }
}